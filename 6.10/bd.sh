#!/bin/bash
cd "${0%/*}"

docker build -t printdotcom/lambda-dev:6.10 .
docker push printdotcom/lambda-dev:6.10